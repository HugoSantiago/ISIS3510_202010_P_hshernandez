//
//  ViewController.swift
//  PCMaster
//
//  Created by Hugo Santiago on 26/02/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var welcomeimage: UIImageView!
    
    @IBAction func mainView(_ sender: Any) {
        performSegue(withIdentifier: "segueWelcome", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

